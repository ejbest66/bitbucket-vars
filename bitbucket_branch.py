# This code sample uses the 'requests' library:
# http://docs.python-requests.org
import requests

username = 'ebest66'  # username or workspace name
repository = 'repository_name'  # name of the repository
app_password = 'ApU7F9YDwkSdRymwyYvb'  # App password create one from Personal settings
branch_name = 'branch_name'  # name of the branch to create
branch_from = 'master'  # You can use different commit hash or branch name
url = f"https://api.:bitbucket.org/2.0/repositories/{username}/{repository}/refs/branches"

response = requests.post(
    url,
    auth=(username, app_password),
    json={"name": branch_name, "target": {"hash": branch_from}}
)

print(response.text)
