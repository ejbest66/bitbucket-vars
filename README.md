# bitbucket-vars #

Programs to do different things with bitbucket vars used in BitBucket Pipelines
<br>
Syntax <br><br>
python3 getVarValue.py ejbest66 ej6

# BitBucket setup app password #
<pre>
1) Your profile and settings in bottom left
2) "Personal Settings"
3) "App Passwords"
4) "Create App Password"
5) Have variables in your project 
</pre>
# Save the file like this #
<pre>
cat ~/.ssh/bitbucket.key 
username = "ejbest66"
password = "xxxxxxxxxxxxxxxxxx"
</pre>
# to getVarValue.py #
<pre> python3 getVarValue.py ejbest66 ej6</pre>

# to setVarValue.py #
<pre> python3 setVarValue.py ejbest66 ej6 var value Y for private</pre>
<pre> python3 setVarValue.py ejbest66 ej6 var value N for clear text</pre>

