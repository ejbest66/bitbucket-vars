# This code sample uses the 'requests' library:
# http://docs.python-requests.org
import requests
import sys
import base64
import json
import re

# in BitBucket to to
#  1) your profile and settings in bottom left
#  2) "Personal Settings"
#  3) "App Passwords"
#  4) "Create App Password"
#
# Syntax python3 setVarValue.py ejbest66 pipelineexample2 mytest mytest Y
#
filewithpath = "/Users/ej/.ssh/bitbucket-vars.key"
if len(sys.argv) < 6:
    print("Incorrect number of command line variables")
    sys.exit()
if sys.argv[5] != "Y" and sys.argv[5] != 'N':
    print("Value for 5th element should be 'Y'(true) or 'N'(false)")
    sys.exit()
secure = True if sys.argv[5] == 'Y' else False
workspace_id = sys.argv[1]
repo = sys.argv[2]
print("workspace id: ", workspace_id)
print("repo name: ", repo)
with open(filewithpath, "r") as f:
    data = f.read()
username = re.findall("username {0,}= {0,}(.*)", data)[0]
password = re.findall("password {0,}= {0,}(.*)", data)[0]
s = username + ":" + password
s = s.encode()
base64encoded = base64.b64encode(s).decode()
url = f"https://api.bitbucket.org/2.0/repositories/{workspace_id}/{repo}/pipelines_config/variables/?pagelen=100"
payload = {}
headers = {"Authorization": f"Basic {base64encoded}"}
response = requests.request("GET", url, headers=headers, data=payload)
jsonData = json.loads(response.text)
for value in jsonData.get("values", []):
    if value.get("key") == sys.argv[3]:
        id = value.get("uuid")
        url = f"https://api.bitbucket.org/2.0/repositories/{workspace_id}/{repo}/pipelines_config/variables/{id}"
        headers = {"Authorization": f"Basic {base64encoded}",
            "Accept": "application/json",
            "Content-Type": "application/json"}

        payload = json.dumps( {
            "type": "pipeline_variable",
            "key": sys.argv[3],
            "value": sys.argv[4],
            "secured": secure,
} )

        response = requests.request(
   "PUT",
   url,
   data=payload,
   headers=headers
)
        print("Variable Updated")
        print(response.json())
        sys.exit(0)

url = f"https://api.bitbucket.org/2.0/repositories/{workspace_id}/{repo}/pipelines_config/variables/"
headers = {"Authorization": f"Basic {base64encoded}",
    "Accept": "application/json",
    "Content-Type": "application/json"}

payload = json.dumps( {
    "type": "pipeline_variable",
    "key": sys.argv[3],
    "value": sys.argv[4],
    "secured": secure,
} )

response = requests.request(
"POST",   
url,
   data=payload,
   headers=headers

)
print("Variable Created")
print(response.json())