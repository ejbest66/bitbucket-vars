import requests
import sys
import base64
import json
import re

# in BitBucket to to
#  1) your profile and settings in bottom left
#  2) "Personal Settings"
#  3) "App Passwords"
#  4) "Create App Password"
#
# Syntax python3 getVarValue.py ejbest66 ej6
#
def getRepositoryVariables(workspace_id, repo, page_no=1):
    with open("/Users/ej/.ssh/bitbucket.key", "r") as f:
        data = f.read()
    username = re.findall("username {0,}= {0,}(.*)", data)[0]
    password = re.findall("password {0,}= {0,}(.*)", data)[0]
    s = username + ":" + password
    s = s.encode()
    base64encoded = base64.b64encode(s).decode()

    url = f"https://api.bitbucket.org/2.0/repositories/{workspace_id}/{repo}/pipelines_config/variables/?page={page_no}"

    payload = {}
    headers = {"Authorization": f"Basic {base64encoded}"}

    response = requests.request("GET", url, headers=headers, data=payload)
    #print(response)

    jsonData = json.loads(response.text)
    for value in jsonData.get("values", []):
        space = 20 - len(value.get("key"))
        print("", value.get("key"), " " * space, "|", "",  value.get("value"))

    if not jsonData.get("values", []):
        return
    getRepositoryVariables(workspace_id, repo, page_no + 1)


if __name__ == "__main__":
    if len(sys.argv) == 3:
        workspace_id = sys.argv[1]
        repo_name = sys.argv[2]
        print("workspace id: ", workspace_id)
        print("repo name: ", repo_name)
        getRepositoryVariables(workspace_id, repo_name)
